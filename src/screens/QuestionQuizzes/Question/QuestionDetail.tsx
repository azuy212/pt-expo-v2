import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { NavigationStackScreenProps } from 'react-navigation-stack';
import { Container, Content, View, H1 } from 'native-base';

import HeaderComponent from '../../../components/HeaderComponent';
import { FilesBaseUrl } from '../../../services/question';
import WebViewFlex from '../../../components/WebViewFlex';
import { WebViewMessageEvent } from 'react-native-webview';

interface IState {
  filePath: string;
  videoPath: string;
  sQuestion: string;
  error: boolean;
}

export default class QuestionDetail extends Component<NavigationStackScreenProps, IState> {
  state = {
    filePath: '',
    videoPath: '',
    sQuestion: 'Question Detail',
    error: false,
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    this.setState(params as IState);
  }

  handleError = (event: WebViewMessageEvent) => {
    const message = event.nativeEvent.data;
    this.setState({ error: message === 'Error' });
  }

  render() {
    const { error, filePath } = this.state;
    return (
      <Container>
        <HeaderComponent {...this.props} title={this.state.sQuestion} />
        <Content contentContainerStyle={{ flex: 1 }}>
          {error ? (
            <View style={styles.error}>
              <H1>No Data Found!</H1>
            </View>
          ) : (
            <WebViewFlex
              style={styles.webView}
              url={`${FilesBaseUrl}/${filePath}`}
              onMessage={this.handleError}
              onError={() => this.setState({ error: true })}
            />
          )}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 10,
  },
  webView: {
    flex: 8,
  },
  error: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
