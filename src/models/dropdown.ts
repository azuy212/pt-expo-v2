interface IDropDownOption {
  label: string;
  value: string;
}

export interface IDropDownOptions extends Array<IDropDownOption> {}
