export interface IQuestion {
  id_question: number;
  class: string;
  course: string;
  subject: string;
  section: string;
  chapter: number;
  no_question: string;
  question: string;
  lecture_link: string;
  lecture_video: string;
  keyword: string;
  flag: number;
  username: string;
  input_time: string;
}
