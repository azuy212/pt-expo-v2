export interface IQuiz {
  id_quizzes: number;
  class: string;
  subject: string;
  course: string;
  chapter: number;
  file_name: string;
  quiz_link: string;
  answer: string;
  det_answer: string;
  lecture_link: string;
  video_link: string;
  keyword: string;
  flag: number;
  upd: string;
  input_time: string;
}
